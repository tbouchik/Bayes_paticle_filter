#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 24 08:06:27 2018

@author: bouchikhi
"""
import copy
import numpy as np
import matplotlib.pyplot as plt
import plotly.plotly as py
from matplotlib.animation import ArtistAnimation
import random
#exemple de modèle de mesure :  

"Incertitude des capteurs"

p_z1_ouvert_sachant_ouvert = 0.6
p_z1_ouvert_sachant_ferme = 0.3

"Probas de l'etat de la porte"
p_ouvert = 0.5
p_ferme = 0.5


"Calcul de la proba de lecture = ouvert :c'est le déonminateur dans la formule de Bayes"

p_lec_ouvert = p_z1_ouvert_sachant_ouvert * p_ouvert + p_z1_ouvert_sachant_ferme * p_ferme
"Calcul de la Prédiction"


p_ouvert_sachant_lec_ouvert = (p_z1_ouvert_sachant_ouvert * p_ouvert) / p_lec_ouvert
# Implantation de la formule de Bayes 
def Bayes_un_capteur (pLec1State1, pLec1State_1, pState1, pState_1) :
    """[calcule une prédiction pour un capteur : probabilité de l'état 1 sachant la lecture de 1 ]
    
    Arguments:
        pLec1State1 {[int]} -- [probabilité conditionnelle de lire 1 sachant l'état 1]
        pLec1State_1 {[int]} -- [probabilité conditionnelle de lire 1 sachant l'état non 1]
        pState1 {[int]} -- [probabilité marginale de l'état 1]
        pState_1 {[int]} -- [probabilité marginale de l'état non 1]
    
    Returns:
        [int] -- [description]
    """
    pLec1 = pLec1State1 * pState1 + pLec1State_1 * pState_1
    pState1Lec1 = (pLec1State1*pState1)/pLec1 
    #print('resultat de Bayes_un_capteur', pState1Lec1)
    return pState1Lec1

print(Bayes_un_capteur(0.6, 0.3, 0.5, 0.5))

def Bayes_deux_capteurs (pZ1Lec1State1, pZ1Lec1State_1, pZ2Lec1State1, pZ2Lec1State_1, pState1, pState_1):
    """[calcule une prédiction pour deux capteurs : probabilité de l'état 1 sachant la lecture de 1 par les deux capteurs]
    
    Arguments:
        pZ1Lec1State1 {[type]} -- [probabilité conditionnelle de lire 1 par z1 sachant l'état 1]
        pZ1Lec1State_1 {[type]} -- [probabilité conditionnelle de lire 1 par z1 sachant l'état non 1]
        pZ2Lec1State1 {[type]} -- [probabilité conditionnelle de lire 1 par z2 sachant l'état 1]
        pZ2Lec1State_1 {[type]} -- [probabilité conditionnelle de lire 1 par z2 sachant l'état non 1]
        pState1 {[type]} -- [probabilité marginale de l'état 1]
        pState_1 {[type]} -- [probabilité marginale de l'état non 1]
    """
    num = pZ2Lec1State1 * Bayes_un_capteur(pZ1Lec1State1, pZ1Lec1State_1, pState1, pState_1 )
    den = num + pZ2Lec1State_1 * Bayes_un_capteur(pZ1Lec1State_1, pZ1Lec1State1 ,pState1, pState_1)
    #print('resultat de Bayes_deux_capteurs', num/den)

    return num/den

print (Bayes_deux_capteurs(0.6, 0.3, 0.5, 0.6, 0.5, 0.5))


#Modélisation de l'action et intégration aux calculs précédents : 
"Les actions augmentent les incertitudes, contrairement au mersures"
"On pousse pour fermer la porte"

#[croyance= ouverte , croyance =ferme] === [ Bayes_deux_capteurs(0.6, 0.3, 0.5, 0.6, 0.5, 0.5), 1-  Bayes_deux_capteurs(0.6, 0.3, 0.5, 0.6, 0.5, 0.5)]
def integration_odometry (probaConditionellesOdometry, pastCroyances):
    """[Renvoie la prédiction après éxécution de l'instruction]
    
    Arguments:
        probaConditionellesOdometry {[array<int>]} -- [le modèle de mouvement]
        pastCroyances {[array<int>]} -- [Croyances à l'état précédent]
    
    les deux arguments doivent être ordonnées dans un sens bien particulier
    Returns:
        [int] -- [Prédiction]
    """

    result = 0

    for k in range(len(pastCroyances)):
        # print ('                ------exec integration ododmetry : ------' )
        # print ('past croyances :  ', pastCroyances)
        # print (' probas conditionnelles : ', probaConditionellesOdometry )
        result += probaConditionellesOdometry[k] * pastCroyances [k]
    # print ('                ------FIN exec integration ododmetry : ------' )
    return result



probaMarginalesPorteOuverte =  [ Bayes_deux_capteurs(0.6, 0.3, 0.5, 0.6, 0.5, 0.5), 1-  Bayes_deux_capteurs(0.6, 0.3, 0.5, 0.6, 0.5, 0.5)]



def fctInitProba(positions):

    for k in range(len(positions)):
        positions[k] = 0.5
    return positions
    

def localisation_Markov_discret(positions, modeleMouvements, odometryData, capteurData):
    """[summary]
    
    Arguments:
        positions {[type]} -- [0,1]
        modeleMouvements {[type]} -- [ [ [1,0], [0,1] ], [ [0.1, 0], [0.9, 1] ] ]
        odometryData {[type]} -- [0,1] "ne rien faire" puis "ouvrir"
        capteurData {[type]} -- [[0.6, 0.2], [0.6, 0.2]] lis ouvert 
    
    Returns:
        [type] -- [description]
    """
    i = 0
    j=0
    positionsProbas = fctInitProba(positions)
    predictions = copy.deepcopy(positionsProbas)
    croyances = copy.deepcopy(positionsProbas)
    #print ('init ', croyances)

    #Tant qu'il y'a des instructions odométriques on les intègre
    while (i < len(odometryData) or j < len(capteurData)):
        
        if i < len(odometryData): 
            for k in range(len(positions)):
                predictions[k] = integration_odometry(modeleMouvements[i][k],croyances) 
            i += 1
        print ("etape", i, " , les predictions: ", predictions)
        if j < len(capteurData): 
            etha = 1/(capteurData[j][0]*predictions[0] + capteurData[j][0-1]*predictions[0-1])
            print ('ethat : ', etha)
            for k in range(len(positions)):
                #print('croyance[',k,'] = ',capteurData[j][k],' * ', predictions[k],'/etha')
                croyances[k] = capteurData[j][k]*predictions[k]*etha
            j +=1
        print ("etape", i, "  ,  les croyances : ", croyances)

    return croyances



print ('--------------execution------------')
print (localisation_Markov_discret([0,1], [ [ [1,0], [0,1] ], [ [1, 0.8], [0, 0.2] ] ],[0,1] , [[0.6, 0.2], [0.6, 0.2]]))

print ('------------------------------*************************--------------------------------')
print ('------------------------- BAYES DISCRETE PARTICLE FILTER ------------------------------')
print ('------------------------------*************************--------------------------------')



#Map 1D
couloir = np.array([1, 1, 0, 0, 0, 0, 0, 0, 1, 0])




def nouvelle_croyance(couloir, croyance, z, rapport_correction):
    for i, val in enumerate(couloir):
        if val == z:
            croyance[i] *= rapport_correction


def normaliser_distribution(croyance) : 
    somme = sum(croyance)
    for i in range(len(croyance)) : 
        croyance[i] /= somme

print ('------------------------Incorporation de déplacements------------------------')
def prediction_deplacement_ideal(croyance, deplacement):
    
    n = len(croyance)
    result = np.zeros(n)
    for i in range(n):
        result[i] = croyance[(i-deplacement) % n]
    return result

def prediction_deplacement(croyance, deplacement=1, p_sous_estim=0.1, p_correct=0.8, p_sur_estim=0.1):
    n = len(croyance)
    result = np.zeros(n)
    for i in range(n):
        result[i] = (
            croyance[(i-deplacement) % n]   * p_correct +
            croyance[(i-deplacement-1) % n] * p_sur_estim +
            croyance[(i-deplacement+1) % n] * p_sous_estim)      
    return result



def prediction__deplacement_adapte_map(couloir, deplacements, p_sous_estim=0.2, p_correct=0.6, p_sur_estim=0.2):
    croyance_etapes=[]
    data = []
    fig= plt.figure()
    croyance = np.array([1./len(couloir)]*len(couloir))
    croyance_etapes.append(copy.deepcopy(croyance)) 
    print('init: ', croyance_etapes)
    nouvelle_croyance(couloir, croyance, 1,5)
    normaliser_distribution(croyance)
    croyance_etapes.append(copy.deepcopy(croyance))
    odometry_sum =0
    for i in range(len(deplacements)):
        
        n = len(croyance)
        result = np.zeros(n)
        if deplacements[i] == 1 : 
            #le random.random()*0.1 simule les erreurs de lecture sur le 0 et le 1 du couloir
            lec1 = couloir[odometry_sum+deplacements[i]] + random.random()*0.1*couloir[odometry_sum+deplacements[i]]
            lec2 = couloir[odometry_sum] +random.random()*0.1*couloir[odometry_sum]
            if (abs(lec1 - lec2) <0.3) and (lec2>0.5) :
                croyance[0] *= 5 #Repère du modèle de mouvement
                croyance = prediction_deplacement(croyance, deplacements[i])
                odometry_sum +=deplacements[i]
                normaliser_distribution(croyance)
                croyance_etapes.append(copy.deepcopy(croyance))
                
            else : 
                croyance = prediction_deplacement(croyance, deplacements[i])
                odometry_sum +=deplacements[i]
                normaliser_distribution(croyance)
                croyance_etapes.append(copy.deepcopy(croyance))
        if deplacements[i] == -1 : 
            #le random.random()*0.1 simule les erreurs de lecture sur le 0 et le 1 du couloir
            lec1 = couloir[odometry_sum+deplacements[i]] + random.random()*0.1*couloir[odometry_sum+deplacements[i]]
            lec2 = couloir[odometry_sum] +random.random()*0.1*couloir[odometry_sum]
            if (abs(lec1 - lec2) <0.3) and (lec2>0.5) :
                croyance[1] *= 5 #Repère du modèle de mouvement
                croyance = prediction_deplacement(croyance, deplacements[i])
                odometry_sum +=deplacements[i]
                normaliser_distribution(croyance)
                croyance_etapes.append(copy.deepcopy(croyance))
            else:
                croyance = prediction_deplacement(croyance, deplacements[i])
                odometry_sum +=deplacements[i]
                normaliser_distribution(croyance)
                croyance_etapes.append(copy.deepcopy(croyance))

    ims = []
    for l in range(len(croyance_etapes)):
        
        plt.axis([0,10,0,2])
        if deplacements == [1,1,1,1,1,-1,-1,-1,-1,-1]: #pour le titre
            plt.title('aller retour')
        elif deplacements == [1,1,1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1]: #pour le titre
            plt.title('le robot reste entre les portes')
        pic = plt.bar(range(len(croyance)), croyance_etapes[l], color='c')

        ims.append(pic)

    anim =ArtistAnimation(fig, ims, interval=500)
    plt.show()          

#___________------------------ SIMULATION ---------------------_______________#
prediction__deplacement_adapte_map(couloir, [1,1,1,1,1,-1,-1,-1,-1,-1], p_sous_estim=0.1, p_correct=0.8, p_sur_estim=0.1)
prediction__deplacement_adapte_map(couloir,[1,1,1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1], p_sous_estim=0.1, p_correct=0.8, p_sur_estim=0.1)

fig = plt.figure()
x_couloir = range(len(couloir))
plt.subplot(221)
plt.title('Repartition des portes')
plt.xlabel("Couloir")
plt.ylabel('Portes')
plt.axis([0,10,0,2])
plt.bar(x_couloir,couloir,1) 

#Représentation de la croyance
croyance = np.array([1./10]*10)
x_croyance = range(len(croyance))

plt.subplot(222)
plt.title('Capteur eteint ')
plt.xlabel("Couloir")
plt.ylabel('Croyance')
plt.axis([0,10,0,0.7])
plt.bar(x_croyance, croyance, 1)
nouvelle_croyance(couloir, croyance, 1, 5)
normaliser_distribution(croyance)

plt.subplot(223)
plt.title('Avant deplacement ')
plt.xlabel("Couloir")
plt.ylabel('Croyance')
plt.axis([0,10,0,0.5])
plt.bar(range(len(croyance)), croyance, 1)

croyance = prediction_deplacement(croyance, 1)
plt.subplot(224)
plt.title('Apres deplacement ')
plt.xlabel("Couloir")
plt.ylabel('Croyance')
plt.axis([0,10,0,0.5])
plt.bar(range(len(croyance)), croyance, 1)
plt.show()



croyance_etapes = []
data = []
fig= plt.figure()
for _ in range(10):
    croyance = prediction_deplacement(croyance, 1)
    croyance_etapes.append(croyance)
for i in range(len(croyance_etapes[0])):
    data.append([croyance_etapes[j][i] for j in range(len(croyance))])
ims = []
for i in range(len(croyance_etapes[0])):
    pic = plt.bar(range(len(croyance)), data[i], color='c')
    ims.append(pic)

anim =ArtistAnimation(fig, ims, interval=200)
plt.show()



